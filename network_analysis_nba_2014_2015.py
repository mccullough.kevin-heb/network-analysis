# Databricks notebook source
# Analyze Cross Shopping of Stores Using Network Analysis
import networkx as nx
from graphframes import GraphFrame
from pyspark.sql.functions import desc
import matplotlib.pyplot as plt
from pyspark.sql.functions import rand
import community
import matplotlib.cm as cm
import pandas as pd
import networkx.algorithms.community  as nx_comm

# COMMAND ----------

file_location = "/Users/m339673/shot_logs.csv"     

nba = (spark.read.format("csv")
    .options(header=True, mergeSchema=True, inferSchema=True)
    .load(file_location))

# COMMAND ----------

nba.createOrReplaceTempView('nba')

# COMMAND ----------

display(spark.sql('select * from nba'))

# COMMAND ----------

df =spark.sql('select concat("Shooter_",cast(player_id as string)) shooter, concat("Defender_",cast(CLOSEST_DEFENDER_PLAYER_ID as string)) defender, case when FGM=0 then PTS_TYPE*-1 when FGM=1 then PTS end as weight from nba').toPandas()

df2 =spark.sql('select concat("Shooter_",cast(player_id as string)) shooter, concat("Defender_",cast(CLOSEST_DEFENDER_PLAYER_ID as string)) defender, sum(case when FGM=1 then PTS end) as weight from nba where FGM=1 group by 1,2').toPandas()

df3 =spark.sql('select concat("Shooter_",cast(player_id as string)) shooter, concat("Defender_",cast(CLOSEST_DEFENDER_PLAYER_ID as string)) defender, sum(case when FGM=0 then PTS_TYPE*1 end) as weight from nba where FGM=0 group by 1,2').toPandas()

# COMMAND ----------

df3.head()

# COMMAND ----------

# DBTITLE 1,Create Bipartite Graph
B = nx.Graph()
B.add_nodes_from(df['shooter'], bipartite='shooter')
B.add_nodes_from(df['defender'], bipartite='defender')
B.add_weighted_edges_from(
    [(row['shooter'], row['defender'], row['weight']) for idx, row in df.iterrows()], 
    weight='weight')

# COMMAND ----------

B2 = nx.Graph()
B2.add_nodes_from(df2['shooter'], bipartite='shooter')
B2.add_nodes_from(df2['defender'], bipartite='defender')
B2.add_weighted_edges_from(
    [(row['shooter'], row['defender'], row['weight']) for idx, row in df2.iterrows()], 
    weight='weight')

# COMMAND ----------

B3 = nx.Graph()
B3.add_nodes_from(df3['shooter'], bipartite='shooter')
B3.add_nodes_from(df3['defender'], bipartite='defender')
B3.add_weighted_edges_from(
    [(row['shooter'], row['defender'], row['weight']) for idx, row in df3.iterrows()], 
    weight='weight')

# COMMAND ----------

# DBTITLE 1,Connectivity Stats
N, K = nx.bipartite.projected_graph(B2, [node for node in B2.nodes() if B2.nodes[node]['bipartite'] == 'shooter']).order(), B2.size()
avg_deg = float(K) / N
print("Nodes: ", N)
print("Edges: ", K)
print("Average degree: ", avg_deg)

# COMMAND ----------

# DBTITLE 1,Shooter Degree Centrality: % of Defenders connected to
shooter_nodes = [node for node in B.nodes() if B.nodes[node]['bipartite'] == 'shooter']

shooter_nodes_with_centrality = [node for node in nx.bipartite.degree_centrality(B, shooter_nodes).items() if node[0].startswith('Shooter')]

foo = pd.DataFrame(sorted(shooter_nodes_with_centrality, key=lambda x: x[1], reverse=True)[:10])
foo.columns=['Shooter','Centrality']
foo['Shooter']=foo.Shooter.str.split('_', expand=True)[1].astype(int)


shooter_df = spark.sql('select distinct player_id, player_name from nba').toPandas()
foo.merge(shooter_df, left_on='Shooter', right_on='player_id')

# COMMAND ----------

foo3 = foo.merge(foo2, left_on="Shooter", right_on="defender")
foo3['avg_centrality']=(foo3.Centrality_x+foo3.Centrality_y)/2

display(foo3.merge(shooter_df, left_on='Shooter', right_on='player_id'))

# COMMAND ----------

foo2.head()

# COMMAND ----------

shooter_nodes = [node for node in B2.nodes() if B2.nodes[node]['bipartite'] == 'shooter']

shooter_nodes_with_centrality = [node for node in nx.bipartite.degree_centrality(B2, shooter_nodes).items() if node[0].startswith('Shooter')]

foo = pd.DataFrame(sorted(shooter_nodes_with_centrality, key=lambda x: x[1], reverse=True))
foo.columns=['Shooter','Centrality']
foo['Shooter']=foo.Shooter.str.split('_', expand=True)[1].astype(int)


shooter_df = spark.sql('select x.*, rank()over(order by points desc) pts_rnk, rank()over(order by shot_count desc) shot_cnt_rnk, rank()over(order by fg_pct desc) fg_pct_rnk from (select distinct player_id, player_name, sum(PTS) points, count(*) shot_count, sum(case when fgm=1 then 1 else 0 end)/count(*) as fg_pct from nba group by 1,2)x').toPandas()
display(foo.merge(shooter_df, left_on='Shooter', right_on='player_id'))

# COMMAND ----------

# DBTITLE 1,Defender Degree Centrality: % of Shooters Connected To
defender_nodes = [node for node in B3.nodes() if B3.nodes[node]['bipartite'] == 'defender']

defender_nodes_with_centrality = [node for node in nx.bipartite.degree_centrality(B3, defender_nodes).items() if node[0].startswith('Defender_')]

sorted(defender_nodes_with_centrality, key=lambda x: x[1], reverse=True)[:10]

foo2 = pd.DataFrame(sorted(defender_nodes_with_centrality, key=lambda x: x[1], reverse=True))
foo2.columns=['defender','Centrality']
foo2['defender']=foo2.defender.str.split('_', expand=True)[1].astype(int)

defender_df = spark.sql('select x.*, rank()over(order by shots_defended desc) shots_defended_rnk, rank()over(order by pct_defended_successfully desc) pct_defended_rnk, rank()over(order by pct_2pt_defended_successfully desc) pct_2pt_defended_rnk, rank()over(order by pct_3pt_defended_successfully desc) pct_3pt_defended_rnk from (select distinct closest_defender_player_id as player_id, closest_defender, count(*) as shots_defended, sum(case when fgm=0 then 1 else 0 end)/count(*) as pct_defended_successfully, sum(case when fgm=0 and pts_type=2 then 1 else 0 end)/sum(case when  pts_type=2 then 1 else 0 end) as pct_2pt_defended_successfully, sum(case when fgm=0 and pts_type=3 then 1 else 0 end)/sum(case when  pts_type=3 then 1 else 0 end) as pct_3pt_defended_successfully from nba group by 1,2)x').toPandas()
display(foo2.merge(defender_df, left_on='defender', right_on='player_id'))

# COMMAND ----------

# DBTITLE 1,Project the Graphs as Unipartite
shooter_graph = nx.bipartite.projected_graph(B2, shooter_nodes)
defender_graph = nx.bipartite.weighted_projected_graph(B3, defender_nodes)

# COMMAND ----------

# DBTITLE 1,Page Rank by Defender
dcs = pd.Series(nx.pagerank(defender_graph))
dcs=pd.DataFrame(dcs.sort_values(ascending=False)[:100].reset_index())
dcs.columns=['Defender', 'page_rank']
dcs['Defender']=dcs.Defender.str.split('_', expand=True)[1].astype(int)
display(dcs.merge(defender_df, left_on='Defender', right_on='player_id'))

# COMMAND ----------

dcs = pd.Series(nx.pagerank(shooter_graph))
dcs=pd.DataFrame(dcs.sort_values(ascending=False).reset_index())
dcs.columns=['Shooter', 'page_rank']
dcs['Shooter']=dcs.Shooter.str.split('_', expand=True)[1].astype(int)
display(dcs.merge(shooter_df, left_on='Shooter', right_on='player_id'))
