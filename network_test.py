# Databricks notebook source
import networkx as nx
import community
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
from graphframes import GraphFrame

# COMMAND ----------

# define the graph
edge = [(1,2),(1,3),(1,4),(1,5),(1,6),(2,7),(2,8),(2,9)]
G = nx.Graph()
G.add_edges_from(edge)

# COMMAND ----------

# retrun partition as a dict
partition = community.best_partition(G)
# draw the graph
pos = nx.spring_layout(G)
# color the nodes according to their partition
cmap = cm.get_cmap('viridis', max(partition.values()) + 1)
nx.draw_networkx_nodes(G, pos, partition.keys(), node_size=40,
                       cmap=cmap, node_color=list(partition.values()))
nx.draw_networkx_edges(G, pos, alpha=0.5)
plt.show()

# COMMAND ----------

partition

# COMMAND ----------

# MAGIC %md
# MAGIC ### Example 2

# COMMAND ----------

import numpy as np
import networkx as nx
np.random.seed(9)

# I will generate a stochastic block model using `networkx` and then extract the weighted adjacency matrix.
sizes = [50, 50, 50] # 3 communities
probs = [[0.25, 0.05, 0.02],
         [0.05, 0.35, 0.07],
         [0.02, 0.07, 0.40]]

# Build the model
Gblock = nx.stochastic_block_model(sizes, probs, seed=0)

# Extract the weighted adjacency
W = np.array(nx.to_numpy_matrix(Gblock, weight='weight'))
W[W==1] = 1.5
print(W.shape)
# (150, 150)

# COMMAND ----------

W

# COMMAND ----------

G = nx.from_numpy_array(W)
louvain_partition = community.best_partition(G, weight='weight')
modularity2 = community.modularity(louvain_partition, G, weight='weight')
print("The modularity Q based on networkx is {}".format(modularity2))

# COMMAND ----------

louvain_partition

# COMMAND ----------

# MAGIC %md
# MAGIC #### Example 3

# COMMAND ----------

# MAGIC %scala
# MAGIC 
# MAGIC 
# MAGIC import org.apache.spark.sql._
# MAGIC import org.apache.spark.sql.functions._
# MAGIC 
# MAGIC import org.graphframes._
# MAGIC 
# MAGIC 
# MAGIC val v = sqlContext.createDataFrame(List(
# MAGIC   ("a", "Alice", 34),
# MAGIC   ("b", "Bob", 36),
# MAGIC   ("c", "Charlie", 30),
# MAGIC   ("d", "David", 29),
# MAGIC   ("e", "Esther", 32),
# MAGIC   ("f", "Fanny", 36),
# MAGIC   ("g", "Gabby", 60)
# MAGIC )).toDF("id", "name", "age")
# MAGIC // Edge DataFrame
# MAGIC val e = sqlContext.createDataFrame(List(
# MAGIC   ("a", "b", "friend"),
# MAGIC   ("b", "c", "follow"),
# MAGIC   ("c", "b", "follow"),
# MAGIC   ("f", "c", "follow"),
# MAGIC   ("e", "f", "follow"),
# MAGIC   ("e", "d", "friend"),
# MAGIC   ("d", "a", "friend"),
# MAGIC   ("a", "e", "friend")
# MAGIC )).toDF("src", "dst", "relationship")

# COMMAND ----------

# MAGIC %scala
# MAGIC val g = GraphFrame(v, e)

# COMMAND ----------

# MAGIC %scala
# MAGIC // display(g.vertices)
# MAGIC display(g.edges)
# MAGIC display(g.inDegrees)

# COMMAND ----------

# MAGIC %scala
# MAGIC val numFollows = g.edges.filter("relationship = 'follow'").count()

# COMMAND ----------

# MAGIC %scala
# MAGIC val result = g.labelPropagation.maxIter(5).run()
# MAGIC display(result.orderBy("label"))

# COMMAND ----------

# MAGIC %scala 
# MAGIC val results = g.pageRank.resetProbability(0.15).tol(0.01).run()
# MAGIC display(results.vertices)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Example 4

# COMMAND ----------

station_location = "/Users/m339673/station.csv"     

station = (spark.read.format("csv")
    .options(header=True, mergeSchema=True, inferSchema=True)
    .load(station_location))


trip_location = "/Users/m339673/trip.csv"     

trip = (spark.read.format("csv")
    .options(header=True, mergeSchema=True, inferSchema=True)
    .load(trip_location))

# COMMAND ----------

trip.createOrReplaceTempView('trip')
station.createOrReplaceTempView('station')

# COMMAND ----------

# MAGIC %scala
# MAGIC val bikeStations = sqlContext.sql("SELECT * FROM station")
# MAGIC val tripData = sqlContext.sql("SELECT * FROM trip")

# COMMAND ----------

display(spark.sql('select * from trip'))

# COMMAND ----------

# MAGIC %scala
# MAGIC import org.apache.spark.sql._
# MAGIC import org.apache.spark.sql.functions._
# MAGIC 
# MAGIC import org.graphframes._

# COMMAND ----------

# MAGIC %scala
# MAGIC val stationVertices = bikeStations
# MAGIC   .distinct()
# MAGIC 
# MAGIC val tripEdges = tripData
# MAGIC   .withColumnRenamed("start_station_name", "src")
# MAGIC   .withColumnRenamed("end_station_name", "dst")
# MAGIC 
# MAGIC 
# MAGIC 
# MAGIC display(stationVertices)

# COMMAND ----------

# MAGIC %scala
# MAGIC display(tripEdges)

# COMMAND ----------

# MAGIC %scala
# MAGIC val stationGraph = GraphFrame(stationVertices, tripEdges)
# MAGIC 
# MAGIC tripEdges.cache()
# MAGIC stationVertices.cache()

# COMMAND ----------

# MAGIC %scala
# MAGIC println("Total Number of Stations: " + stationGraph.vertices.count)
# MAGIC println("Total Number of Trips in Graph: " + stationGraph.edges.count)
# MAGIC println("Total Number of Trips in Original Data: " + tripData.count)// sanity check

# COMMAND ----------

# MAGIC %scala
# MAGIC val topTrips = stationGraph
# MAGIC   .edges
# MAGIC   .groupBy("src", "dst")
# MAGIC   .count()
# MAGIC   .orderBy(desc("count"))
# MAGIC   .limit(10)
# MAGIC 
# MAGIC display(topTrips)

# COMMAND ----------

# MAGIC %scala
# MAGIC val inDeg = stationGraph.inDegrees
# MAGIC display(inDeg.orderBy(desc("inDegree")).limit(5))

# COMMAND ----------

# MAGIC %scala
# MAGIC val outDeg = stationGraph.outDegrees
# MAGIC display(outDeg.orderBy(desc("outDegree")).limit(5))

# COMMAND ----------

# MAGIC %scala
# MAGIC val degreeRatio = inDeg.join(outDeg, inDeg.col("id") === outDeg.col("id"))
# MAGIC   .drop(outDeg.col("id"))
# MAGIC   .selectExpr("id", "double(inDegree)/double(outDegree) as degreeRatio")
# MAGIC 
# MAGIC degreeRatio.cache()
# MAGIC   
# MAGIC display(degreeRatio.orderBy(desc("degreeRatio")).limit(10))

# COMMAND ----------

# MAGIC %scala
# MAGIC display(degreeRatio.orderBy(asc("degreeRatio")).limit(10))

# COMMAND ----------

# MAGIC %scala 
# MAGIC val results = stationGraph.pageRank.resetProbability(0.15).tol(0.01).run()
# MAGIC display(results.vertices)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Example 5

# COMMAND ----------

# MAGIC %scala
# MAGIC val bikeStations = sqlContext.sql("SELECT id FROM station")
# MAGIC val tripData = sqlContext.sql("SELECT start_station_id, end_station_id FROM trip")

# COMMAND ----------

# MAGIC %scala
# MAGIC val stationVertices = bikeStations
# MAGIC   .distinct()
# MAGIC 
# MAGIC val tripEdges = tripData
# MAGIC   .withColumnRenamed("start_station_id", "src")
# MAGIC   .withColumnRenamed("end_station_id", "dst")

# COMMAND ----------

# MAGIC %scala
# MAGIC val g = GraphFrame(stationVertices, tripEdges)
# MAGIC 
# MAGIC tripEdges.cache()
# MAGIC stationVertices.cache()

# COMMAND ----------

# MAGIC %scala
# MAGIC // display(g.vertices)
# MAGIC display(g.edges)
# MAGIC display(g.inDegrees)

# COMMAND ----------

# MAGIC %scala
# MAGIC val result = g.labelPropagation.maxIter(10).run()
# MAGIC display(result.orderBy("label"))

# COMMAND ----------

# MAGIC %scala
# MAGIC display(result)

# COMMAND ----------

# MAGIC %scala
# MAGIC result.createOrReplaceTempView("result")

# COMMAND ----------

# MAGIC %scala
# MAGIC val foo = sqlContext.sql("select a.*, b.label from station a inner join result b on a.id=b.id")
# MAGIC 
# MAGIC display(foo)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Example 6

# COMMAND ----------

import networkx as nx
from graphframes import GraphFrame

def PlotGraph(edge_list):
    Gplot=nx.Graph()
    for row in edge_list.select('src','dst').take(1000):
        Gplot.add_edge(row['src'],row['dst'])

    plt.subplot(121)
    nx.draw(Gplot, with_labels=True, font_weight='bold')

vertices = sqlContext.createDataFrame([
  ("a", "Alice", 34),
  ("b", "Bob", 36),
  ("c", "Charlie", 30),
  ("d", "David", 29),
  ("e", "Esther", 32),
("e1", "Esther2", 32),
  ("f", "Fanny", 36),
  ("g", "Gabby", 60),
    ("h", "Mark", 61),
    ("i", "Gunter", 62),
    ("j", "Marit", 63)], ["id", "name", "age"])

edges = sqlContext.createDataFrame([
  ("a", "b", "friend"),
  ("b", "a", "follow"),
  ("c", "a", "follow"),
  ("c", "f", "follow"),
  ("g", "h", "follow"),
  ("h", "i", "friend"),
  ("h", "j", "friend"),
  ("j", "h", "friend"),
    ("e", "e1", "friend")
], ["src", "dst", "relationship"])

g = GraphFrame(vertices, edges)
PlotGraph(g.edges)

# COMMAND ----------

Gplot=nx.Graph()
for row in g.edges.select('src','dst').take(1000):
  Gplot.add_edge(row['src'],row['dst'])

# draw the graph
pos = nx.spring_layout(Gplot)
# color the nodes according to their partition
nx.draw_networkx_nodes(Gplot, pos)
nx.draw_networkx_edges(Gplot, pos, alpha=0.5)
plt.show()

# COMMAND ----------

Gplot=nx.Graph()
for row in g.edges.select('src','dst').take(1000):
  Gplot.add_edge(row['src'],row['dst'])
  
nx.draw(Gplot, with_labels = True)

# COMMAND ----------

# MAGIC %md 
# MAGIC #### Example 7

# COMMAND ----------

bikeStations = spark.sql("SELECT * FROM station")
tripData = spark.sql("SELECT * FROM trip")

# COMMAND ----------

stationVertices = spark.sql('select distinct id, name, lat, long, dock_count, city, installation_date from station')

tripEdges = spark.sql('select id, start_station_name as src, end_station_name as dst from trip')

# COMMAND ----------

g = GraphFrame(stationVertices, tripEdges)

# COMMAND ----------

Gplot=nx.Graph()
for row in g.edges.select('src','dst').take(100000):
  Gplot.add_edge(row['src'],row['dst'])
  
plt.figure(1,figsize=(18,8))
nx.draw_spring(Gplot, with_labels = True)

# COMMAND ----------

from graphframes.examples import Graphs
g = Graphs(sqlContext).friends()  # Get example graph

# Run PageRank until convergence to tolerance "tol".
results = g.pageRank(resetProbability=0.15, tol=0.01)
# Display resulting pageranks and final edge weights
# Note that the displayed pagerank may be truncated, e.g., missing the E notation.
# In Spark 1.5+, you can use show(truncate=False) to avoid truncation.
results.vertices.select("id", "pagerank").show()
results.edges.select("src", "dst", "weight").show()

# Run PageRank for a fixed number of iterations.
results2 = g.pageRank(resetProbability=0.15, maxIter=10)

# Run PageRank personalized for vertex "a"
results3 = g.pageRank(resetProbability=0.15, maxIter=10, sourceId="a")

# Run PageRank personalized for vertex ["a", "b", "c", "d"] in parallel
results4 = g.parallelPersonalizedPageRank(resetProbability=0.15, sourceIds=["a", "b", "c", "d"], maxIter=10)

# COMMAND ----------

# MAGIC %md
# MAGIC #### Example 8

# COMMAND ----------

ncaa_location = "/Users/m339673/ncaa2021.csv"     

ncaa = (spark.read.format("csv")
    .options(header=True, mergeSchema=True, inferSchema=True)
    .load(ncaa_location))

# COMMAND ----------

display(ncaa)

# COMMAND ----------

ncaa = ncaa.toPandas()

# COMMAND ----------

vertices = pd.DataFrame(pd.concat([ncaa['Loser'], ncaa['Winner']], axis=0).unique(), columns=['id'])
vertices=spark.createDataFrame(vertices)

# COMMAND ----------

edges = ncaa.rename(columns={'Loser':'src', 'Winner':'dst'})
edges=spark.createDataFrame(edges)

# COMMAND ----------

g = GraphFrame(vertices, edges)

# COMMAND ----------

# Run PageRank until convergence to tolerance "tol".
results = g.pageRank(resetProbability=0.15, tol=0.000001)

# COMMAND ----------

display(results.vertices.select("id", "pagerank"))

# COMMAND ----------

display(g.inDegrees)

# COMMAND ----------

community_result = g.labelPropagation(maxIter=3)   

community_result.createOrReplaceTempView('communities')
display(spark.sql('select a.* from communities a'))

# COMMAND ----------

Gplot=nx.Graph()
for row in g.edges.select('src','dst').take(100000):
  Gplot.add_edge(row['src'],row['dst'])
  
plt.figure(1,figsize=(18,8))
nx.draw_spring(Gplot, with_labels = True)
