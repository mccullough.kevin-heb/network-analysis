# Databricks notebook source
# Analyze Cross Shopping of Stores Using Network Analysis
import networkx as nx
from graphframes import GraphFrame
from pyspark.sql.functions import desc
import matplotlib.pyplot as plt
from pyspark.sql.functions import rand
import community
import matplotlib.cm as cm
import pandas as pd
import networkx.algorithms.community  as nx_comm

# COMMAND ----------

# DBTITLE 1,Filters
spark.sql('''select distinct key_store
          from serve.v_dim_store a
          where cd_retl_loc_frmt <>'CNV'
          and dsc_str not like '%TRAINING%'
          and id_lob in (32001) and id_rgn=2
          and dt_str_open is not null
          and dt_str_close is null''').createOrReplaceTempView('store_df')



spark.sql('''select distinct a.id_date, a.id_fscl_wk
    from
    serve.dim_date a
    inner join
    serve.fscl_wk b on a.id_fscl_wk=b.fscl_wk_id
    where wks_from_today_nbr between -12 and -1''').createOrReplaceTempView('date_df')


spark.sql('''select distinct id_cat, id_prod, id_consm_unt_cd as id_upc
  from serve.dim_product_all 
  where id_prod <>0
    and id_dept not in (0,00,1,5,8, 11, 15, 99, 21)
    and id_cat not in (0)''').createOrReplaceTempView('product_df')


# COMMAND ----------

# DBTITLE 1,Eligible Households
# Households with at least 3 transactions overall
spark.sql('''select hh.livg_unt_id, count(distinct a.id_shopping_trans) trans_cnt
            from ampdb.v_customerorders a
            inner join
            ampdb.v_consolidated_ccrn_hh hh on a.instnc_id=hh.instnc
            inner join
            date_df d on a.id_date=d.id_date
            inner join
            store_df e on a.key_store=e.key_store
            inner join
            product_df f on a.id_prod=f.id_prod
            group by 1
            having count(distinct a.id_shopping_trans)>=3''').createOrReplaceTempView('hh_w_min_transactions_overall')

# COMMAND ----------

# DBTITLE 1,Eligible Households with Transaction Order
# HH transasctions in order
spark.sql('''select distinct hh.livg_unt_id, a.key_store, a.id_date, a.id_time_part
            from ampdb.v_customerorders a
            inner join
            ampdb.v_consolidated_ccrn_hh hh on a.instnc_id=hh.instnc
            inner join
            date_df d on a.id_date=d.id_date
            inner join
            store_df e on a.key_store=e.key_store
            inner join
            product_df f on a.id_prod=f.id_prod
            inner join
            hh_w_min_transactions_overall g on hh.livg_unt_id=g.livg_unt_id''').createOrReplaceTempView('hh_w_min_transactions')

# COMMAND ----------

# DBTITLE 1,Cross Join Each Store that a household shopped
#Cross each store
spark.sql('''select distinct a.livg_unt_id, a.key_store, b.key_store as key_store_b
          from hh_w_min_transactions a
          left join
          hh_w_min_transactions b on a.livg_unt_id=b.livg_unt_id
          ''').createOrReplaceTempView('hh_combs')

# Apply this filter if you only want edges where there was a change in stores:
# and a.key_store!=b.key_store

# COMMAND ----------

# DBTITLE 1,Distinct count of households per store
# Vertices
spark.sql('''select key_store, count(distinct livg_unt_id) hh_cnt 
            from hh_combs
            group by key_store
          ''').createOrReplaceTempView('store_hh_cnt')

# COMMAND ----------

# DBTITLE 1,Distinct count of households for each store combination
# Edges
spark.sql('''select key_store, key_store_b, count(distinct livg_unt_id) hh_cnt 
            from hh_combs
            group by key_store, key_store_b
          ''').createOrReplaceTempView('store_store_hh_cnt')

# COMMAND ----------

# DBTITLE 1,Conditional probability of shopping at a store at least once given another store
store_probs = spark.sql('''select c.dsc_str, a.key_store, d.dsc_str as dsc_str_b, b.key_store_b, a.hh_cnt, b.hh_cnt as hh_cnt_b, b.hh_cnt/a.hh_cnt as pct_cross
          from
          store_hh_cnt a
          inner join
          store_store_hh_cnt b on a.key_store=b.key_store
          left join
          serve.dim_store c on a.key_store=c.key_store
          left join
          serve.dim_store d on b.key_store_b=d.key_store
          where b.key_store_b is not null
         ''')

store_probs.write.mode("overwrite").format("delta").saveAsTable("eds.store_probs")

# COMMAND ----------

# DBTITLE 1,SA43 Probability of Cross Shop
display(spark.sql('select * from eds.store_probs where key_store=20753'))

# COMMAND ----------

display(spark.sql('select count(*) from eds.edges where key_store=20753'))

# COMMAND ----------

# DBTITLE 1,SA43 Probability Given Store Switching Ocurrences
display(spark.sql('''select b.dsc_str, key_store_b, count(*)/1308.00 as pct_cross from eds.edges a inner join
                  serve.dim_store b on a.key_store_b=b.key_store where a.key_store=20753 group by 1,2'''))

# COMMAND ----------

# MAGIC %md
# MAGIC ### Network Analysis with Graphframes

# COMMAND ----------

# DBTITLE 1,Randomly Sample 1% of Households
# Random sample households
hhs = spark.sql('select livg_unt_id from hh_w_min_transactions_overall')
hh_sample = hhs.sample(False, 0.01, seed=0)
hh_sample.cache()
hh_sample.createOrReplaceTempView('hh_sample')

# COMMAND ----------

# DBTITLE 1,HH Sample Transactions in Order
# HH transasctions in order
spark.sql('''select distinct hh.livg_unt_id, a.key_store, a.id_date, a.id_time_part
            from ampdb.v_customerorders a
            inner join
            ampdb.v_consolidated_ccrn_hh hh on a.instnc_id=hh.instnc
            inner join
            date_df d on a.id_date=d.id_date
            inner join
            store_df e on a.key_store=e.key_store
            inner join
            product_df f on a.id_prod=f.id_prod
            inner join
            hh_sample g on hh.livg_unt_id=g.livg_unt_id''').createOrReplaceTempView('hh_transactions')


## Ordering trips
spark.sql('''select a.*, rank()over(partition by livg_unt_id order by id_date, id_time_part) as trp_ord 
            from hh_transactions a
          ''').createOrReplaceTempView('trans_order')

# COMMAND ----------

# DBTITLE 1,Vertices and Edges
# Vertices
vertices=spark.sql('select distinct key_store as id from store_hh_cnt')
vertices.write.mode("overwrite").format("delta").saveAsTable("eds.vertices")


# Edges 
edges = spark.sql('''select a.livg_unt_id, a.key_store, b.key_store as key_store_b, a.trp_ord, b.trp_ord as trp_ord_b from trans_order a
          inner join
          trans_order b on a.livg_unt_id=b.livg_unt_id and a.trp_ord+1=b.trp_ord
          ''')

#where a.key_store!=b.key_store

edges.write.mode("overwrite").format("delta").saveAsTable("eds.edges")


edges= spark.sql('select key_store as src, key_store_b as dst from eds.edges')

# COMMAND ----------

# DBTITLE 1,Create GraphFrame graph
g = GraphFrame(vertices, edges)
vertices.cache()
edges.cache()

# COMMAND ----------

# DBTITLE 1,Show Vertices and Edges
display(g.vertices)
display(g.edges)

# COMMAND ----------

# DBTITLE 1,Most Common Combinations of Nodes based on Edge Counts
# Most common combinations of nodes based on edge counts
g.edges.groupBy("src", "dst").count().sort(desc("count")).createOrReplaceTempView('edges_count')

display(spark.sql('''select b.dsc_str, a.src as key_store, c.dsc_str as dsc_str_b, a.dst as key_store_b, a.count
                  from edges_count a 
                  left join 
                  serve.dim_store b on a.src=b.key_store
                  left join
                  serve.dim_store c on a.dst=c.key_store
                  order by count desc'''))

# COMMAND ----------

# DBTITLE 1,In Degrees
# Number of edges going into a vertices
in_degree = g.inDegrees
in_degree.createOrReplaceTempView('in_degree')

display(spark.sql('select b.dsc_str, a.id as key_store, a.inDegree from in_degree a inner join serve.dim_store b on a.id=b.key_store'))

# COMMAND ----------

# DBTITLE 1,Out Degrees
# Number of edges going into other vertices
out_degree = g.outDegrees
out_degree.createOrReplaceTempView('out_degree')

display(spark.sql('select b.dsc_str, a.id as key_store, a.outDegree from out_degree a inner join serve.dim_store b on a.id=b.key_store'))

# COMMAND ----------

# DBTITLE 1,In Degree: Out Degree Ratio
# Degree ratio
display(spark.sql('''select c.dsc_str, a.id as key_sstore, a.inDegree/b.outDegree as degree_ratio 
from in_degree a 
inner join 
out_degree b on a.id=b.id
inner join
serve.dim_store c on a.id=c.key_store'''))

# COMMAND ----------

# DBTITLE 1,Page Rank with a 15% Probability of Reset
results = g.pageRank(resetProbability=0.15, maxIter=5)

results.vertices.select("id", "pagerank").createOrReplaceTempView('page_rank')

display(spark.sql('select b.dsc_str, a.id as key_store, a.pagerank from page_rank a inner join serve.dim_store b on a.id=b.key_store'))

# COMMAND ----------

# Run PageRank personalized for single vertex
results2 = g.pageRank(resetProbability=0.15, maxIter=5, sourceId="20753")

results2.vertices.select("id", "pagerank").createOrReplaceTempView('page_rank2')

display(spark.sql('select b.dsc_str, a.id as key_store, a.pagerank from page_rank2 a inner join serve.dim_store b on a.id=b.key_store'))

# COMMAND ----------

community_result = g.labelPropagation(maxIter=3)   

community_result.createOrReplaceTempView('communities')
display(spark.sql('select b.dsc_str, a.id as key_store, a.label from communities a inner join serve.dim_store b on a.id=b.key_store order by label'))

# COMMAND ----------

#Attempt to visualize
Gplot=nx.Graph()
for row in g.edges.select('src','dst').orderBy(rand()).take(500):
  Gplot.add_edge(row['src'],row['dst'])
  
plt.figure(1,figsize=(18,8))
nx.draw(Gplot, with_labels = True)

# COMMAND ----------

# MAGIC %md
# MAGIC ### Convert to networkx

# COMMAND ----------

# DBTITLE 1,Sample edges and plot a community graph
Gplot=nx.Graph()
for row in g.edges.select('src','dst').orderBy(rand()).take(10000):
  Gplot.add_edge(row['src'],row['dst'])

# Louvain Community Detection
# retrun partition as a dict
partition = community.best_partition(Gplot)
# draw the graph
pos = nx.spring_layout(Gplot)
# color the nodes according to their partition
cmap = cm.get_cmap('viridis', max(partition.values()) + 1)
plt.figure(1,figsize=(18,8))
nx.draw_networkx_nodes(Gplot, pos, partition.keys(), node_size=40,
                       cmap=cmap, node_color=list(partition.values()))
nx.draw_networkx_edges(Gplot, pos, alpha=0.5)


# COMMAND ----------

# DBTITLE 1,Reload graph with all data and print summary statistics
Gplot=nx.from_pandas_edgelist(edges.toPandas(),'src','dst', create_using=nx.DiGraph())

partition = community.best_partition(Gplot.to_undirected())

N, K = Gplot.order(), Gplot.size()
avg_deg = float(K) / N
print("Nodes: ", N)
print("Edges: ", K)
print("Average degree: ", avg_deg)
print("SCC: ", nx.number_strongly_connected_components(Gplot))
print("WCC: ", nx.number_weakly_connected_components(Gplot))

# COMMAND ----------

louvain_dict = community.generate_dendrogram(Gplot.to_undirected())

louvain= pd.DataFrame(list(louvain_dict[0].items()),columns = ['key_store','group']) 

# COMMAND ----------

louvain.merge(store_df, left_on='key_store', right_on='key_store').sort_values('group', ascending=False).head()

# COMMAND ----------

# DBTITLE 1,Asynchronous Fluid Community Detection
list(nx_comm.asyn_fluidc(Gplot.to_undirected(),k=5))


# COMMAND ----------

# DBTITLE 1,Average Clustering Coefficient
# Clustering Component
cam_net_ud = Gplot.to_undirected()

print(nx.average_clustering(cam_net_ud))

# COMMAND ----------

# DBTITLE 1,Centrality Metrics
# Centrality
# Betweenness centrality
bet_cen = nx.betweenness_centrality(cam_net_ud)
# Closeness centrality
clo_cen = nx.closeness_centrality(cam_net_ud)


# Convert dictionaries to dataframes
df = pd.DataFrame(list(bet_cen.items()),columns = ['key_store','bet_cen']) 
df2 = pd.DataFrame(list(clo_cen.items()),columns = ['key_store','clo_cen']) 

# Store details
store_df = spark.sql('select distinct key_store, dsc_str, lat_k, lon_k from serve.dim_store').toPandas()

# Merge centrality metrics with stores
#display(df.merge(store_df, left_on='key_store', right_on='key_store').sort_values('bet_cen', ascending=False))
display(df2.merge(store_df, left_on='key_store', right_on='key_store').sort_values('clo_cen', ascending=False))
