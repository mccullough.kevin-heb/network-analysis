# Databricks notebook source
# Analyze Cross Shopping of Stores Using Network Analysis
import networkx as nx
from graphframes import GraphFrame
from pyspark.sql.functions import desc
import matplotlib.pyplot as plt
from pyspark.sql.functions import rand
import community
import matplotlib.cm as cm
import pandas as pd
import networkx.algorithms.community  as nx_comm

# COMMAND ----------

# DBTITLE 1,Filters
spark.sql('''select distinct key_store
          from serve.v_dim_store a
          where cd_retl_loc_frmt <>'CNV'
          and dsc_str not like '%TRAINING%'
          and id_lob in (32001) and id_rgn=2
          and dt_str_open is not null
          and dt_str_close is null''').createOrReplaceTempView('store_df')



spark.sql('''select distinct a.id_date, a.id_fscl_wk
    from
    serve.dim_date a
    inner join
    serve.fscl_wk b on a.id_fscl_wk=b.fscl_wk_id
    where wks_from_today_nbr between -12 and -1''').createOrReplaceTempView('date_df')


spark.sql('''select distinct id_cat, id_prod, id_consm_unt_cd as id_upc
  from serve.dim_product_all 
  where id_prod <>0
    and id_dept not in (0,00,1,5,8, 11, 15, 99, 21)
    and id_cat not in (0)''').createOrReplaceTempView('product_df')


# COMMAND ----------

# DBTITLE 1,Eligible Households
# Households with at least 3 transactions overall
spark.sql('''select hh.livg_unt_id, count(distinct a.id_shopping_trans) trans_cnt
            from ampdb.v_customerorders a
            inner join
            ampdb.v_consolidated_ccrn_hh hh on a.instnc_id=hh.instnc
            inner join
            date_df d on a.id_date=d.id_date
            inner join
            store_df e on a.key_store=e.key_store
            inner join
            product_df f on a.id_prod=f.id_prod
            group by 1
            having count(distinct a.id_shopping_trans)>=3''').createOrReplaceTempView('hh_w_min_transactions_overall')

# COMMAND ----------

# DBTITLE 1,Randomly Sample 1% of Households
# Random sample households
hhs = spark.sql('select livg_unt_id from hh_w_min_transactions_overall')
hh_sample = hhs.sample(False, 0.01, seed=0)
hh_sample.cache()
hh_sample.createOrReplaceTempView('hh_sample')

# COMMAND ----------

# DBTITLE 1,Eligible Households with Transaction Order
# HH transasctions in order
foo = spark.sql('''select distinct hh.livg_unt_id, a.key_store, a.id_date
            from ampdb.v_customerorders a
            inner join
            ampdb.v_consolidated_ccrn_hh hh on a.instnc_id=hh.instnc
            inner join
            date_df d on a.id_date=d.id_date
            inner join
            store_df e on a.key_store=e.key_store
            inner join
            product_df f on a.id_prod=f.id_prod
            inner join
            hh_sample g on hh.livg_unt_id=g.livg_unt_id''')

foo.write.format("delta").mode("overwrite").saveAsTable("eds.test")

# COMMAND ----------

# DBTITLE 1,Convert to Pandas DataFrame
hh_store_df = spark.sql('select concat("Store_",cast(key_store as string)) key_store, concat("HH_",cast(livg_unt_id as string)) livg_unt_id, count(*) as weight from eds.test where livg_unt_id>0 group by 1,2').toPandas()

store_df=spark.sql('select key_store, dsc_str, lat_k, lon_k, RETL_LOC_FRMAT_DES, RETL_LOC_SEGM_CD from serve.dim_store').toPandas()

hh_df_sel=spark.sql('select a.livg_unt_id, min(tier_12wk_cd) tier_12wk_cd, min(master_segment) master_segment from ampdb.v_consolidated_ccrn_hh a inner join eds.test b on a.livg_unt_id=b.livg_unt_id group by 1 ').toPandas()

# COMMAND ----------

hh_store_df.head()

# COMMAND ----------

# DBTITLE 1,Create Bipartite Graph
B = nx.Graph()
B.add_nodes_from(hh_store_df['key_store'], bipartite='store')
B.add_nodes_from(hh_store_df['livg_unt_id'], bipartite='household')
B.add_weighted_edges_from(
    [(row['livg_unt_id'], row['key_store'], row['weight']) for idx, row in hh_store_df.iterrows()], 
    weight='weight')

# COMMAND ----------

# DBTITLE 1,Connectivity Stats
N, K = nx.bipartite.projected_graph(B, [node for node in B.nodes() if B.nodes[node]['bipartite'] == 'household']).order(), B.size()
avg_deg = float(K) / N
print("Nodes: ", N)
print("Edges: ", K)
print("Average degree: ", avg_deg)

# COMMAND ----------

list(B.edges(data=True))[:5]

# COMMAND ----------

list(B.nodes(data=True))[:5]

# COMMAND ----------

# DBTITLE 1,Store Degree Centrality: % of Households connected to
store_nodes = [node for node in B.nodes() if B.nodes[node]['bipartite'] == 'store']

store_nodes_with_centrality = [node for node in nx.bipartite.degree_centrality(B, store_nodes).items() if node[0].startswith('Store')]

foo = pd.DataFrame(sorted(store_nodes_with_centrality, key=lambda x: x[1], reverse=True)[:10])
foo.columns=['Store','Centrality']
foo['Store']=foo.Store.str.split('_', expand=True)[1].astype(int)
display(foo.merge(store_df, left_on='Store', right_on='key_store'))

# COMMAND ----------

# DBTITLE 1,Household Degree Centrality: % of Stores Connected To
hh_nodes = [node for node in B.nodes() if B.nodes[node]['bipartite'] == 'household']

hh_nodes_with_centrality = [node for node in nx.bipartite.degree_centrality(B, hh_nodes).items() if node[0].startswith('HH_')]

sorted(hh_nodes_with_centrality, key=lambda x: x[1], reverse=True)[:10]

foo2 = pd.DataFrame(sorted(hh_nodes_with_centrality, key=lambda x: x[1], reverse=True)[:10])
foo2.columns=['livg_unt_id','Centrality']
foo2['livg_unt_id']=foo2.livg_unt_id.str.split('_', expand=True)[1].astype(int)


hh_df = spark.sql('select livg_unt_id, count(distinct instnc) instnc_cnt, count(distinct append_adr_id)adr_cnt, sum(instnc_sales_ex_12wk) sales, min(tier_12wk_cd) loyalty_tier, max(outlier_sw) outlier_sw  from serve.consolidated_ccrn_hh where livg_unt_id in (232863, 11878, 7673850, 1007604651, 15437, 5341297,1025182, 12277878, 1001490215, 5798230) group by 1').toPandas()

display(foo2.merge(hh_df, left_on='livg_unt_id', right_on='livg_unt_id'))

# COMMAND ----------

# DBTITLE 1,Project the Graphs as Unipartite
hh_graph = nx.bipartite.projected_graph(B, hh_nodes)
store_graph = nx.bipartite.weighted_projected_graph(B, store_nodes)

# COMMAND ----------

# DBTITLE 1,Page Rank by Store
dcs = pd.Series(nx.pagerank(store_graph))
dcs=pd.DataFrame(dcs.sort_values(ascending=False)[:10].reset_index())
dcs.columns=['Store', 'page_rank']
dcs['Store']=dcs.Store.str.split('_', expand=True)[1].astype(int)
display(dcs.merge(store_df, left_on='Store', right_on='key_store'))

# COMMAND ----------

# DBTITLE 1,Store Clustering Coefficient
store_nodes = [node for node in B.nodes() if B.nodes[node]['bipartite'] == 'store']

store_nodes_with_clustering = [node for node in nx.bipartite.clustering(B, store_nodes).items() if node[0].startswith('Store')]

sorted(store_nodes_with_clustering, key=lambda x: x[1], reverse=True)


foo = pd.DataFrame(sorted(store_nodes_with_clustering, key=lambda x: x[1], reverse=True)[:10])
foo.columns=['Store','Clustering']
foo['Store']=foo.Store.str.split('_', expand=True)[1].astype(int)
display(foo.merge(store_df, left_on='Store', right_on='key_store'))

# COMMAND ----------

# DBTITLE 1,Personalized Page Rank
import operator
ppr = nx.pagerank(store_graph, personalization={"Store_21535": 1})

ppr = sorted(ppr.items(), key=operator.itemgetter(1), reverse=True)
ppr=pd.DataFrame(ppr)

ppr.columns=['Store', 'page_rank']
ppr['Store']=ppr.Store.str.split('_', expand=True)[1].astype(int)
display(ppr.merge(store_df, left_on='Store', right_on='key_store'))
#for item, score in ppr:
#    print(item, score)

# COMMAND ----------

# DBTITLE 1,Louvain Community Level 0
dendo = community.generate_dendrogram(store_graph)
print("partition at level", 0,
  "is", community.partition_at_level(dendo, 0))

# COMMAND ----------

# DBTITLE 1,Louvain Community Level 1: Best Partition in this Case
print("partition at level", 1,
  "is", community.partition_at_level(dendo, 1))

# COMMAND ----------

# DBTITLE 1,Best Partitions
louvain_dict = community.best_partition(store_graph)

# COMMAND ----------

# DBTITLE 1,Louvain Communities
comms = pd.DataFrame.from_dict(louvain_dict, orient='index').reset_index()
comms.columns=['Store', 'community']
comms['Store']=comms.Store.str.split('_', expand=True)[1].astype(int)
display(comms.merge(store_df, left_on='Store', right_on='key_store'))

# COMMAND ----------

# DBTITLE 1,Create subgraph of a community
H = store_graph.subgraph(["Store_20714",
"Store_20645",
"Store_20854",
"Store_20494",
"Store_20646",
"Store_21535",
"Store_20478"])


subgraph = pd.DataFrame.from_dict(nx.pagerank(H),orient='index').reset_index()
subgraph.columns=['Store','page_rank']
subgraph['Store']=subgraph.Store.str.split('_', expand=True)[1].astype(int)
display(subgraph.merge(store_df, left_on='Store', right_on='key_store'))

# COMMAND ----------

# DBTITLE 1,Plot subgraph with edge weights
pos = nx.spring_layout(H)
plt.figure(1,figsize=(18,8))
nx.draw(H,pos,with_labels=True)
labels = nx.get_edge_attributes(H,'weight')
nx.draw_networkx_edge_labels(H,pos,edge_labels=labels)
